import { cloneElement, useEffect, useMemo, useRef, useState } from "react";
import Inertia from "@/component/Inertia";
import styles from "./index.less";

// 组件属性Props
type Props = {
  list: any[]; // 虚拟列表组件数据，实际数据
  children?: JSX.Element; // 子节点 ｜ 内容
  strength?: number; // 惯性强度， 默认为 10，强度越大，惯性滚动距离越远
  buffer?: number; // 缓冲区比例，默认为 1
  toBottom?: () => void; // 触摸底部边界时触发
};

// 延时器
let timeout: NodeJS.Timeout | null;

const VirtualList = (props: Props) => {
  const { strength, children, list } = props;
  const buffer = props.buffer ?? 1;

  // 列表项节点
  const itemRef = useRef<HTMLDivElement>(null);

  // 是否正在滚动
  const [isScroll, setIsScroll] = useState(false);

  // 列表项尺寸
  const [itemSize, setItemSize] = useState([0, 0]);

  // 视图尺寸 -- 宽高默认为 1 避免列表项内容无法挂载
  const [size, setSize] = useState([1, 1]);

  // 当前视图列表起始索引
  const [index, setIndex] = useState(0);

  // 上缓冲区数量
  const [aboveCount, setAboveCount] = useState(0);

  // 可视数据列表
  const [viewList, setViewList] = useState<typeof list>([]);

  // 设置列表项高度
  useEffect(() => {
    const { offsetWidth, offsetHeight } = itemRef.current ?? {};

    // 当前列表项高度为 0 时设置。
    itemSize.includes(0) && setItemSize([offsetWidth ?? 0, offsetHeight ?? 0]);
  }, [itemRef.current]);

  // 计算选项项列数, 若未获得itemSize时，默认为 1
  const cols = useMemo(
    () => Math.floor(size[0] / (itemSize[0] || size[0])),
    [itemSize, size]
  );

  // 计算视图可显示数量
  const viewCount = useMemo(
    () => Math.ceil(size[1] / (itemSize[1] || 1)) * cols,
    [itemSize, size, cols]
  );

  // 计算真实内容高度
  const realHeight = useMemo(
    () => Math.ceil(list.length / cols) * itemSize[1],
    [itemSize, list, cols]
  );

  // 计算y轴偏移量
  const offsetY = useMemo(
    () =>
      `translate3d(0, ${Math.floor(
        ((index - aboveCount) * itemSize[1]) / cols
      )}px, 0)`,
    [itemSize, index, aboveCount, cols]
  );

  // 计算可视数据列表 && 缓冲区
  useEffect(() => {
    // 缓冲区数量
    const bufferCount = viewCount * buffer;

    // 计算上/下缓冲区数量，根据当前位置选取可用的缓冲区数量
    const aboveCountVal = Math.min(index, bufferCount);
    const belowCount = Math.min(list.length - (index + viewCount), bufferCount);

    // 设置可视区域列表
    // 上缓冲区起始位置：index - aboveCountVal
    // 下缓冲区结束位置：index + viewCount + belowCount
    setViewList(
      list.slice(index - aboveCountVal, index + viewCount + belowCount)
    );

    // 记录上缓冲区数量
    setAboveCount(aboveCountVal);
  }, [index, viewCount, list]);

  // 列表字内容
  const Item = (props: any) => (
    <div
      ref={itemRef}
      id={`virtua_item_${props.index}`}
      className={styles.view_item}
    >
      {cloneElement(children as JSX.Element, props)}
    </div>
  );

  // 惯性滚动事件
  const scroll = (point: any, onBorder: any) => {
    // 根据滑动距离计算起始索引值
    setIndex(Math.floor(Math.abs(point.y) / (itemSize[1] || 1)) * cols);

    // 设置滚动中状态
    setIsScroll(true);

    //在 100ms 后不触发滚动则修改滚动状态
    timeout && clearTimeout(timeout);

    timeout = setTimeout(setIsScroll, 100, false);

    // 判断是否触摸下边界
    onBorder.y && props?.toBottom?.();

    // 判断是否以触摸上下边界
    (onBorder.y || !point.y) && setIsScroll(false);
  };

  // 点击事件转发
  const onClickHandle = (e: any) => {
    // 判断非点击遮罩层则不处理，避免点击事件转发冒泡
    if (!(e.target as HTMLElement).className.includes(styles.real_area)) {
      return false;
    }

    // 获取当前点击坐标位置
    const { top: scrollTop } = e.currentTarget?.getBoundingClientRect?.() ?? {};
    const { pageX, pageY } = e as MouseEvent;
    const [width, height] = itemSize;
    const pointY = Math.abs(scrollTop) + pageY;
    // 根据点击坐标位置计算当前点击位置所属的项 索引
    const offsetX = Math.floor(pageX / (width || 1));
    const id = Math.floor(pointY / (height || 1)) * cols + offsetX;
    const item = document.querySelector(`#virtua_item_${id}>*`) as HTMLElement;
    // 获取元素可点击位置
    const { top, right, bottom, left } = item.getBoundingClientRect();

    // 判断是否点击有效位置 && 将点击事件转发至对应的项上
    [pageX - left, right - pageX, pageY - top, bottom - pageY].every(
      (x) => x > 0
    ) && item.click();
  };

  return (
    <>
      <Inertia listen strength={strength} getSize={setSize} scroll={scroll}>
        {/* 列表真实高度区域 -- 用于出发滚动 */}
        <div
          className={styles.real_area}
          style={{ height: realHeight }}
          onClick={(e) => !isScroll && onClickHandle(e)}
        >
          {/* 列表内容展示区域 */}
          {children && (
            <div style={{ transform: offsetY }}>
              {viewList.map((x, i) => {
                // 计算当前项index，使用该值当作列表渲染key
                const key = index + i - aboveCount;

                return <Item item={x} index={key} key={key} />;
              })}
            </div>
          )}
        </div>
      </Inertia>
    </>
  );
};

export default VirtualList;
