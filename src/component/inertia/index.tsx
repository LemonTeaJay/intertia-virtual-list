import { useEffect, useRef, useState, useTransition } from "react";
import ElementResizeDetector from "element-resize-detector";
import styles from "./index.less";

// 组件属性Props
type Props = {
  children?: JSX.Element | string; // 子节点 ｜ 内容
  strength?: number; //惯性强度， 默认为 10，强度越大，惯性滚动距离越远
  listen?: boolean; //是否监听区域内容size变化，默认 false
  distance?: number; // 距离右/下边界多远时为触底，默认为0
  getSize?: (size: number[]) => void; // 获取视图宽高，入参为[width, height]的size数组
  scroll?: (point?: Point, onBorder?: OnBorder) => void; // 滚动位置事件
};

// 位置点
type Point = {
  x: number;
  y: number;
};

// 是否触碰边界
type OnBorder = {
  x: boolean;
  y: boolean;
};

// 创建元素resize监听器
const erd = ElementResizeDetector();

// 惯性滚动组件
const Intertia = (props: Props) => {
  // 使用过度状态
  const [_isPending, startTransition] = useTransition();

  // 惯性滚动视窗节点
  const intertiaRef = useRef<HTMLDivElement>(null);
  // 滚动内容节点
  const contentRef = useRef<HTMLDivElement>(null);

  // 最大滚动距离
  const [maxMove, setMaxMove] = useState<Point>({ x: 0, y: 0 });

  // 滚动位置
  const [point, setPoint] = useState<Point>({ x: 0, y: 0 });

  // 最后触摸位置 -- 仅记录最后两次触摸位置
  const [touchPoint, setTouchPoint] = useState<Point[]>([{ x: 0, y: 0 }]);

  // 触摸时间 -- 仅记录最后两次触摸位置更变事件
  const [touchTime, setTouchTime] = useState<number[]>([0]);

  // 惯性滚动定时器
  const [timer, setTimer] = useState<NodeJS.Timer | number | null>(null);

  // 最大宽高发生变化时更新滚动位置
  useEffect(() => {
    const { x: maxX, y: maxY } = maxMove;
    const { x, y } = point;

    // 若当前位置超出最大滚动位置则设定为最大滚动位置
    updatePosition({
      x: x > maxX ? x : maxX,
      y: y > maxY ? y : maxY,
    });
  }, [maxMove]);

  // 设置最大x,y轴滚动距离
  useEffect((): (() => void) => {
    // 获取当前视图宽高
    const { offsetWidth, offsetHeight } = intertiaRef.current ?? {};
    // 获取当前内容节点
    const contentEl = contentRef.current as HTMLElement;
    // 获取监听状态
    const isListen = props.listen ?? false;

    // 向上传出试图宽高
    props?.getSize?.([offsetWidth ?? 0, offsetHeight ?? 0]);

    // 若未启动监听，则在此获取当前最大宽高
    !isListen && updateMaxPoint();

    // 判断是否需要监听元素size变化
    isListen && erd.listenTo(contentEl, updateMaxPoint);

    // 判断是否需要移除监听元素size变化
    return () => isListen && erd.removeListener(contentEl, updateMaxPoint);
  }, [contentRef, intertiaRef]);

  // 更新最大x,y轴滚动距离
  const updateMaxPoint = () => {
    // 获取当前视图宽高
    const { offsetWidth, offsetHeight } = intertiaRef.current ?? {};
    // 获取当前内容宽高
    const { offsetWidth: width, offsetHeight: height } =
      contentRef.current ?? {};
    // 计算当前最大滚动距离
    const [x, y] = [
      (offsetWidth ?? 0) - (width ?? 0),
      (offsetHeight ?? 0) - (height ?? 0),
    ];

    // 最大滚动距离取负数。用于translate定位
    setMaxMove({
      x: x > 0 ? 0 : x,
      y: y > 0 ? 0 : y,
    });
  };

  // 更新移动位置
  const updatePoint = (
    axis: "x" | "y",
    move: number,
    currentPoint?: number
  ) => {
    // 根据当前轴位置计算新位置，若不存在当前位置则使用对应轴上的位置
    const newPoint = (currentPoint ?? point[axis]) + move;
    // 判断左/上方向移动是否已到最左/上
    const plusMove = () => (newPoint >= 0 ? 0 : newPoint);
    // 判断右/下方向移动是否已到最右/下
    const minusMove = () =>
      newPoint <= maxMove[axis] ? maxMove[axis] : newPoint;

    // 判断当前轴是左/上还 是 下/右移动
    return (move >= 0 ? plusMove : minusMove)();
  };

  // 更新速度
  const updateSpeed = (speed: number, isPlus: boolean) => {
    // 左/上移动，判断速度是否即将为0
    const plusMove = () => (speed - 1 >= 0 ? speed - 1 : 0);
    // 右/下移动，判断速度是否即将为0
    const minusMove = () => (speed + 1 <= 0 ? speed + 1 : 0);

    // 根据速度方向对速度进行更新
    return (isPlus ? plusMove : minusMove)();
  };

  // 更新当前位置
  const updatePosition = (point: Point) => {
    // 当前轴是否处于边界。当前轴可移动(最大移动距离不为0) && 当前位置 == 最大移动距离
    const onBorder = (axis: "x" | "y") =>
      !!maxMove[axis] && point[axis] - maxMove[axis] <= (props?.distance ?? 0);

    setPoint(point);

    // 触发滚动事件，传入当前位置
    props?.scroll?.(point, { x: onBorder("x"), y: onBorder("y") });
  };

  // 触摸开始事件
  const touchStart = (event: any) => {
    const { pageX, pageY } = (event?.touches as TouchList)?.[0] ?? {};

    // 清除正在进行的惯性滚动
    timer && clearInterval(timer as number);

    setTouchPoint(
      (timer ? [{ x: 0, y: 0 }] : touchPoint).concat({ x: pageX, y: pageY })
    );
    setTouchTime((timer ? [0] : touchTime).concat(Date.now()));
  };

  // 触摸移动事件
  const touchMove = (event: any) => {
    const { pageX, pageY } = (event?.touches as TouchList)?.[0] ?? {};
    const startPoint = touchPoint.at(-1) ?? { x: 0, y: 0 };

    startTransition(() => {
      // 更新x,y轴位置
      updatePosition({
        x: updatePoint("x", pageX - startPoint.x),
        y: updatePoint("y", pageY - startPoint.y),
      });

      // 更新触摸点起始位置，触摸起始时间
      setTouchPoint([startPoint].concat({ x: pageX, y: pageY }));
      setTouchTime([touchTime.at(-1) ?? 0].concat(Date.now()));
    });
  };

  // 触摸结束事件
  const touchEnd = () => {
    const moveTime = touchTime[1] - touchTime[0];
    const strength = props?.strength ?? 10;
    // 获取当前位置
    let { x, y } = point,
      // 计算当前速度
      speedX = ((touchPoint[1].x - touchPoint[0].x) * strength) / moveTime,
      speedY = ((touchPoint[1].y - touchPoint[0].y) * strength) / moveTime,
      // 设置定时器
      interval = setInterval(() => {
        // 计算新速度
        const newSpeedX = updateSpeed(speedX, speedX >= 0);
        const newSpeedY = updateSpeed(speedY, speedY >= 0);
        // 计算新距离
        const newX = updatePoint("x", (speedX + newSpeedX) / 2, x);
        const newY = updatePoint("y", (speedY + newSpeedY) / 2, y);

        // 更新位置
        startTransition(() => {
          updatePosition({ x, y });
        });

        // 更新当前速度 && 当前位置
        [speedX, speedY] = [newSpeedX, newSpeedY];
        [x, y] = [newX, newY];

        // 判断x,y轴速度是否均为0
        if (!speedX && !speedY) {
          clearInterval(interval);

          setTouchPoint([{ x: 0, y: 0 }]);
          setTouchTime([0]);
        }
      }, 17); // 设定17毫秒相当于 60帧的滚动变化速度

    setTimer(interval);
  };

  return (
    <>
      <div
        ref={intertiaRef}
        className={styles.intertia}
        onTouchStart={touchStart}
        onTouchMove={touchMove}
        onTouchEnd={touchEnd}
      >
        {/* 渲染子元素 */}
        <div
          ref={contentRef}
          className={styles.content}
          style={{ transform: `translate3d(${point.x}px, ${point.y}px, 0px)` }}
        >
          {props.children}
        </div>
      </div>
    </>
  );
};

export default Intertia;
