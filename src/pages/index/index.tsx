import { useEffect, useState } from "react";
import VirtualList from "@/component/VirtualList";
import styles from "./index.less";

const getList = (len: number) =>
  Array.from({ length: 100 }, (_x, i) => `第 ${len + i + 1} 项`);

const IndexPage = () => {
  const [list, setList] = useState(getList(0));

  const toBottom = () => setList([...list].concat(getList(list.length)));

  const onClick = (item: string) => alert(`您点击了 ${item}`);

  const Item = ({ item, index }: any) => (
    <div
      className={styles.item}
      style={{ marginTop: index < 2 ? "0.2rem" : 0 }}
      onClick={() => onClick(item)}
    >
      <img className={styles.item_img} src={require("@/assets/yay.jpg")} />
      <div className={styles.item_label}>{item}</div>
    </div>
  );

  useEffect(() => {
    document.title = `列表总数:${list.length}`;
  }, [list]);

  return (
    <>
      <div className={styles.virtual_list}>
        <VirtualList list={list} toBottom={toBottom}>
          <Item />
        </VirtualList>
      </div>
    </>
  );
};

export default IndexPage;
